package com.bhas.dao;

import org.springframework.stereotype.Repository;

@Repository
public class MsgDAO
{
    // constructor,
    public MsgDAO()
    {
        System.out.println("MsgDAO class :: Constructor");
    }

    public void testMethod()
    {
        System.out.println("Trying to invoke the method by IOC,");
    }
}
