package com.bhas;

import com.bhas.dao.MsgDAO;
import com.bhas.one.GM;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MainClass
{
    public static void main(String[] args)
    {
        System.out.println("\n*************\n");
        ApplicationContext context = new ClassPathXmlApplicationContext("Beans2.xml");
                            context.getBean(MsgDAO.class).testMethod();
    }
}
